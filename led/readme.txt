Hello led

Activates a GPIO channel turning on a led on the protoboard.

gpio ------ led ------ 1K ------ ground

wiringpilib Example using the wiring pi library.
helloled    Uses linux device filesystem to access the GPIO.Eclipse project.
linuxfs     C++ that defines a class for the linux device file, a class for the
            gpio and the main program just instantiting a gpio and turning it
            on or off.
