# Configuration file for Raspberry Pi 2 environment
#
# Usage: 
#   In your ~/.bashrc file include this file. Define first the variable RASPDIR
#   pointing to the directory of your Raspberry Pi 2 tooling.
#
#   Example:
#       # Raspberry pi 2 environment
#       RASPDIR=~/work/raspberry
#       if [ -f ${RASPDIR}/raspenv.sh ]; then
#           . ${RASPIR}/raspenv.sh
#       fi
#
# Victor Garcia <vichor@gmail.com>
   
# Set path
if [ -z ${RASPDIR+x} ]; then
    echo "raspenv.sh -- error: RASPDIR envvar not set."
    echo
else
    export RASPDIR
    PATH=${PATH}:${RASPDIR}/tools.git/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin
    export PATH
    alias gccarm='arm-linux-gnueabihf-gcc'
fi
