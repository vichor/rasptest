# README #

This repositorry contains tests performed during the Raspberry Pi 2 training. They are not nice-to-see projects but useful while learning to work with the Rasp.

I am using an Ubuntu host to develop these projects. The idea is to cross compile from the Ubuntu to the raspberry card. Before going straight to the projects, be sure that your box is able to cross compile for ARM/Rasp

## Setup ##

### Development environmenton on your host ###

* [Ubuntu](http://www.ubuntu.com), [Debian](http://www.debian.org), [Linux Mint](http://www.linuxmint.com)
* [Setup generic build environment](https://help.ubuntu.com/community/CompilingEasyHowTo)
* [Setup Git](https://help.ubuntu.com/lts/serverguide/git.html). No need to install gitolite, as bitbucket is our git remote. You may also want to check the [git tutorial](https://www.atlassian.com/git/).
* [Setup cross compiler](http://www.raspberrypi.org/documentation/linux/kernel/building.md), check the INSTALL TOOLCHAIN section and disregard the other kernel related stuff.
* [Setting up Eclipse](http://hertaville.com/2012/09/28/development-environment-raspberry-pi-cross-compiler) to work remotely with the raspberry. The link gives instructions about Kepler Eclipse version. You may use other version if you want (Luna, for instance).
* TODO Remote GDB debugging

### Basic Raspberry setup ###

* [SSH configuration](http://www.linuxproblem.org/art_9.html) for passwordless connections.
* [Install WIFI dongle driver](http://www.raspberrypi.org/forums/viewtopic.php?f=288&t=62371) on the raspberry and [configure the wifi network](http://www.raspyfi.com/wi-fi-on-raspberry-pi-a-simple-guide/).
* [Alternative WIFI setup & configuration](http://www.gc-linux.org/wiki/WL%3AWifi_Configuration).


## The projects ##

### Hello ###

This project is just used to verify the correctness of the raspberry development environment. If arm rasp compiler and ssh service are properly installed and configured in your development computer, you will be able to compile and upload the executable to your rasp computer, log to it and run it there.

To do so:

```
#!bash
$ cd your_hello_directory
$ make
$ make install
$ ssh pi@your_raspberry
[raspberry] $ cd prj
[raspberry] $ ./hello
Hello world Raspberry Pi 2 in C++!!
[raspberry] $ 
```
### Hello Led ###

This project is the LED version of the hello world program. It blinks a led connected to a GPIO pin.
[TODO] Upload code and add explanation here

### Hello Driver ###

This project is the Kernel Module Driver version of the hello world program. When finished, you will have a module that can be installed by means of the modprobe command resulting in a hello world greeting. It can then be uninstalled by means of modprobe -r command, resulting in a goodbye world message.

Building a driver is not a fast task though. Below are explained all the steps followed. They are divided into two main sections: compiling and installing a new kernel (with no wifi loose) and developing, compiling and installing a custom driver.

But first create a directory structure to hold all the sources we are going to need.
```
$ mkdir -p prj/hidrv downloads/kernel.git downloads/rtl8188eu.git
```
Now let's go to build the kernel and our drivers...

**Building and installing a kernel **

Download the linux kernel and the WIFI driver sources. We need to recompile the wifi driver as the one installed so far is for the standard rasp kernel and a driver is intimately linked with a kernel version.
```
$ cd downloads
$ git clone https://github.com/raspberrypi/linux kernel.git
$ git clone https://github.com/lwfinger/rtl8188eu rtl8188eu.git
```

Get the kernel configuration file from your rasp and extract it. The SSH commands below (scp stands for SSH copy) will use rasp as the hostname of the rasp card. Use your own name or the IP address. They also require your user to be added in the raspberry sytem and having privileges; you may use the standard "pi" user instead.
```
$ cd kernel.git
$ scp rasp:/proc/config.gz .
(or)
$ scp pi@your.raspberry.pi.IP:/proc/config.gz .
$ gunzip -c config.gz > .config
```

We are about to compile the kernel. We need to fill up new configuration options on the config file though. We will use default values for the new kernel options.
```
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- olddefconfig
```
You can also try to check which new options are present on the kernel and configure them to your needs. Use the target menuconfig for this instead of the olddefconfig target. You will need the development curses library package installed. This is a tedious task, though, not recommended at this point, but if you want to do it anyway, follow below two steps.
```
$ sudo apt-get install libncurses5-dev
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig
```

Now it's time to compile the kernel. It will take some time. You can use the option -j n, where n is 1.5 times your number of cores, to speed up here. When finished, we will copy the results to the rasp board. 
```
$ make -j 6 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- 
$ make -j 6 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=./modules  modules_install
$ cd arch/arm/boot
$ scp Image rasp:/tmp/kernel7.img
$ cd ../../../modules
$ tar cvzf modules.tgz .
$ scp modules.tgz rasp:/tmp
$ cd ..
$ rm -rf modules
```

Before installing this new kernel into the rasp, compile the wifi driver and transfer it to the rasp board. We will need to transfer the firmware of the wifi dongle as well.
```
$ cd ../rtl8188eu.git
$ make -j 6 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- KSRC=../kernel.git  modules
$ scp 8188eu.ko rasp:/tmp
$ scp rtl8188eufw.bin rasp:/tmp
$ cd ../..
```

Ready to install and test the new kernel and driver. We will backup current working kernel just in case some problem arises. To install the wifi driver, you need to know which kernel version you have just installed. This can be easily seen when decompressing the modules: pay attention to something like 3.18.11-v7+.
```
$ ssh rasp
[rasp] $ sudo -i
[rasp] # cd /boot
[rasp] # cp kernel7.img kernel7original.img
[rasp] # cp /tmp/kernel7.img .
[rasp] # cd /
[rasp] # tar xvzf tmp/modules.tgz
[rasp] # cd /tmp
[rasp] # install -p -m 644 8188eu.ko /lib/modules/3.18.11-v7+/kernel/drivers/net/wireless
[rasp] # cp rtl8188eufw.bin /lib/firmware/.
[rasp] # cp rtl8188eufw.bin /lib/firmware/rtlwifi
[rasp] # depmod -a 3.18.11-v7+
```
The time of thruth. If something goes wrong, remove the SD card, put it on a PC and copy the file kernel7original.img into kernel7.img to recover previous working kernel version.
```
[rasp] # sync
[rasp] # reboot
```

**Build and install a custom driver**

The code for the hello world driver is already available at the repo. Get it now if you have not done it earlier. 
```
$ cd prj
$ git clone https://vichor@bitbucket.org/vichor/rasptest.git rasp.git
```
We have downloaded it into rasp.git directory which is under prj directory. This prj directory is at the same level as the downloads one (the one containing kernel.git). This is important for the paths defined in the Makefiles. You may edit them if you download kernel and hello driver into different directories.

The directories are seen then as follows:

* <work>/downloads/kernel.git
* <work>/prj/rasp.git

Take a look at the driver. To fully understand it check [this book](http://it-ebooks.info/book/339/)

Compile your driver and transfer the driver to your rasp board. The supplied makefile already defines the ARCH and CROSS_COMPILE variables. This makefile will look for a compiled kernel at ../../../downloads/kernel.git
```
$ make modules
$ scp hidrv.ko rasp:/tmp
```

Install your driver in the rasp board. You will need to remember now which was your kernel version. Use it instead of 3.18.11-v7+ below.
```
$ ssh rasp
[rasp] $ sudo -i
[rasp] # cd /
[rasp] # mkdir /lib/modules/3.18.11-v7+/extra
[rasp] # cp tmp/hidrv.ko lib/modules/3.18.11-v7+/extra
[rasp] # depmod -a 3.18.11-v7+
```

You can test your driver now. We will monitor the kernel log file, as there is where the printk output of our driver will appear
```
[rasp] # tail -f /var/log/kern.log &
[rasp] # modprobe hidrv
Apr 13 17:28:46 raspberrypi kernel: [ 5168.116074] Hello, world
[rasp] # lsmod | grep hidrv
hidrv                    790  0 
[rasp] # modprobe -r hidrv
Apr 13 17:29:25 raspberrypi kernel: [ 5207.739421] Goodbye, cruel world
```

Congratulations, you have just run your first Raspberry Linux driver!


## Who do I talk to? ##

* Victor Garcia <vichor@gmail.com>